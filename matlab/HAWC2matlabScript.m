% Matlab controller loop
while time <= Tend
    StringFromHawc=[];
  while (isempty(StringFromHawc))
       StringFromHawc = read(TCPOBJ);
       StringFromHawc = char(StringFromHawc);
  end
    % Replaces NaN with 0 if needed
    StringFromHawc = regexprep(StringFromHawc, 'NAN', '0');
    % Convert string to array
    ArrayFromHawc = str2num(StringFromHawc);
    % Remove first entry from array
    ArrayFromHawc = ArrayFromHawc(2:end);
%%%%%%%%%%%%%%%%%%%%% Get Measurement from HAWC2 %%%%%%%%%%%%%%  
    time = ArrayFromHawc(1); % 1: time              [s]
    omega = ArrayFromHawc(2); % 2: rotor speed       [rad/s]
    theta_bar = ArrayFromHawc(3); % 3: Pitch angle reference of blade 1 from DTU Basic Controller [rad]
    Qe_bar = ArrayFromHawc(4); % 6: Generator torque reference from DTU Basic Controller [Nm]
    Mb1 = ArrayFromHawc(5);% 8: blade1 root 1 Mx       [kNm]
    Mb2 = ArrayFromHawc(8);% 11: blade1 root 2 Mx       [kNm] 
    Mb3 = ArrayFromHawc(11);% 14: blade1 root 3 Mx        [kNm] 
    % check simulation status
    %clc;
%     fprintf('----------------------------------'); fprintf('\n');  
%     fprintf('Simulation time: %3.2f',time); fprintf('\n');
%     fprintf('Power: %3.2f',power); fprintf('\n');
%     fprintf('Rotor speed: %3.2f',omega); fprintf('\n');
%     fprintf('Col_Pitch: %3.2f',theta_bar1*180/pi); fprintf('\n');
%     fprintf('GenTorque: %3.2f',Qe_bar); fprintf('\n');   
%%%%%%%%%%%%%% Design your controller here ! %%%%%%%%%%%%%% 
    theta1 = 0; % additional pithc angle
    theta2 = 0;
    theta3 = 0;
       
%%%%%%%%%%%%%%%%%%% Signals to HAWC2 output %%%%%%%%%%%%%%%%%
    ArrayToHawc     = zeros(40,1); 
   
   ArrayToHawc(1)  = theta_bar+theta1;%  2: Pitch angle reference of blade 1         [rad]
   ArrayToHawc(2)  = theta_bar+theta2;%  3: Pitch angle reference of blade 2         [rad]
   ArrayToHawc(3)  = theta_bar+theta3;%  4: Pitch angle reference of blade 3         [rad]
   ArrayToHawc(4)  = Qe_bar;%  1: Generator torque reference               [Nm]
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
    StringToHawc    = [sprintf('%0.12f;',ArrayToHawc) '*'];
    write(TCPOBJ,int8(StringToHawc));
end

