%% Main file for the Matlab and HAWC2 interface
clc; fclose all; clear all; 

%% Initisation
% kill all processes
system('c:\Windows\System32\taskkill.exe /f /im hawc2mb.exe');
system('c:\Windows\System32\taskkill.exe /f /im cmd.exe');

% Simulation set-up
time = 0;
Tend = 600; % Simulation length

TCPportnumber = 1239;

% tic-toc measures computation time of simulation
tic
cd(['../hawc2_htc' ]);% Go to hawc2 and htc fodler
% call HAWC2 
!hawc2MB.exe ./htc/DTU_10MW_RWT.htc &  

cd('../matlab');

TcpIpConnected = 0;
for i = 1:100
    if TcpIpConnected
        break,
    end
    pause(.1);
    try
       TCPOBJ = tcpclient('127.0.0.1',TCPportnumber,'ConnectTimeout',30);
        TcpIpConnected = 1;
    catch TryTcpIpConnect
        TcpIpConnected = 0;
        fprintf('.')
    end
end

HAWC2matlabScript 

toc

% end
