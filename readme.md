Matlab-HAWC2 Interface
======================

The Matlab-HAWC2 interfance enables you to build your controller in Matlab and calls HAWC2 in the Matlab environment. The folder also contains a working example of the [DTU10MW Reference Wind Turbine
Model](http://www.hawc2.dk/Download/HAWC2-Model/DTU-10-MW-Reference-Wind-Turbine). 


Installation
============

This repository is written for Windows.

The code [HAWC2](http://www.hawc2.dk/) should be downloaded.

Set up the environment variable in Windows to use HAWC2 on the command line. Namely, you should be able to call `hawc2mb.exe` from cmd.

The turbine model (htc, data, control folders) should be placed in hawc2_htc folder as well. A working exmaple of DTU 10MW model is provided.


Example
=======

To begin, go into the "matlab" folder.

Run "Main.m"

If you want to design your own controller, go to "HAWC2matlabScript.m", where you can place your own controller. By default, the Matlab script will bypass the control actions from the [DTU Wind Energy Controller (DTUWEC)](https://gitlab.windenergy.dtu.dk/OpenLAC/BasicDTUController).


Contact
=====
Feel free to contact me at wali@dtu.dk if you encounter any problems regarding this interface.
